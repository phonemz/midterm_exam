class CreateReaders < ActiveRecord::Migration[7.0]
  def change
    create_table :readers do |t|
      t.string :readername
      t.text :comment

      t.timestamps
    end
  end
end
